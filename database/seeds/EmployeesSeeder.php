<?php

use App\Models\EmployeeModel;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Vitor Ecorban',
            'email' => 'vitor@ecorban.com.br',
            'login' => 'vitorecorban',
            'password' => bcrypt('123456'),
        ]);
    
        User::create([
            'name' => 'Matheus Picioli',
            'email' => 'matheus.picioli98@gmail.com',
            'login' => 'mpicioli',
            'password' => bcrypt('123456'),
        ]);
        
        for($i = 0; $i < 20; $i++) {
            $user = User::create([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'login' => Str::random(10),
                'password' => bcrypt('123456'),
            ]);
            $employee = new EmployeeModel([
                'description_function'  => Str::random(10),
                'salary'                => (float)rand(1000, 10000),
                'date_birthday'         => rand(1950, 2020).'/'.rand(10,12).'/'.rand(1,31)
            ]);
            $employee->user()->associate($user);
            $employee->save();
        }
    }
}
