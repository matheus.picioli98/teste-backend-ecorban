<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogModel extends Model
{
    use SoftDeletes;
    
    protected $table = 'logs';
    protected $fillable = [
        'class',
        'event',
        'data',
        'class_id'
    ];
    
    protected $casts = [
        'class_id'  => 'integer',
        'data'      => 'array'
    ];
}
