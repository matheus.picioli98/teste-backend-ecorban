<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeModel extends Model
{
    use SoftDeletes;

    protected $table = 'employees';
    protected $fillable = [
        'description_function',
        'salary',
        'date_birthday',
        'user_id', // Maldito fillable atrapalhando meu teste
    ];

    protected $casts = [
        'salary' => 'float'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
