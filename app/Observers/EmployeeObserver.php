<?php

namespace App\Observers;

use App\Models\EmployeeModel;
use App\Models\LogModel;

class EmployeeObserver
{
    /**
     * Handle the employee model "created" event.
     *
     * @param  \App\Models\EmployeeModel  $employeeModel
     * @return void
     */
    public function created(EmployeeModel $employeeModel)
    {
        LogModel::create([
            'class'     => EmployeeModel::class,
            'class_id'  => $employeeModel->id,
            'event'     => 'created',
            'data'      => $employeeModel->toJson()
        ]);
    
    }

    /**
     * Handle the employee model "updated" event.
     *
     * @param  \App\Models\EmployeeModel  $employeeModel
     * @return void
     */
    public function updated(EmployeeModel $employeeModel)
    {
        LogModel::create([
            'class'     => EmployeeModel::class,
            'class_id'  => $employeeModel->id,
            'event'     => 'updated',
            'data'      => $employeeModel->toJson()
        ]);
    }

    /**
     * Handle the employee model "deleted" event.
     *
     * @param  \App\Models\EmployeeModel  $employeeModel
     * @return void
     */
    public function deleted(EmployeeModel $employeeModel)
    {
        LogModel::create([
            'class'     => EmployeeModel::class,
            'class_id'  => $employeeModel->id,
            'event'     => 'deleted',
            'data'      => $employeeModel->toJson()
        ]);
    }
}
