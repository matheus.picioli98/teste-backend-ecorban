<?php

namespace App\Providers;

use App\Models\EmployeeModel;
use App\Observers\EmployeeObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        EmployeeModel::observe(EmployeeObserver::class);
    }
}
