<?php

namespace App\Http\Controllers;

use App\Models\EmployeeModel;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;


class EmployeesController extends Controller
{
    public function index()
    {
        $employees = EmployeeModel::with('user')->paginate(5);
        return response()->json($employees, Response::HTTP_OK);
    }


    public function create()
    {
        //
    }
    
    public function store(StoreEmployeeRequest $request)
    {
        $employee_info = $request->get('employee_info');
        $user_info = $request->get('user_info');
        $user = new User([
            'name'      => $user_info['name'],
            'email'     => $user_info['email'],
            'login'     => $user_info['login'],
            'password'  => bcrypt($user_info['password']),
        ]);
        
        try {
            $user->save();
            $employee = EmployeeModel::create([
                'salary'                => $employee_info['salary'],
                'date_birthday'         => $employee_info['date_birthday'],
                'description_function'  => $employee_info['description_function'],
                'user_id'               => $user->id,
            ]);
            return response()->json([
                'user'      => $user,
                'employee'  => $employee
            ], Response::HTTP_CREATED);
        } catch (QueryException $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
                'status'    => $exception->getCode()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        //
    }
    
    public function update(UpdateEmployeeRequest $request, $id)
    {
        try {
            $employee = EmployeeModel::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
                'status'    => $exception->getCode()
            ], Response::HTTP_NOT_FOUND);
        }
        
        $employee_info = $request->get('employee_info');
        
        try {
            $employee->update([
                'salary'                => $employee_info['salary'],
                'date_birthday'         => $employee_info['date_birthday'],
                'description_function'  => $employee_info['description_function'],
            ]);
            $user_info = $request->get('user_info');
            $user = $employee->user;
            $user->update([
                'name'      => $user_info['name'],
                'email'     => $user_info['email'],
                'login'     => $user_info['login'],
            ]);
            
            return response()->json([
                'user'      => $user,
                'employee'  => $employee,
            ], Response::HTTP_OK);
        } catch (QueryException $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
                'status'    => $exception->getCode()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function destroy($id)
    {
        try {
            $employee = EmployeeModel::findOrFail($id);
            $user = $employee->user;
            
            $user->delete();
            $employee->delete();
            return response()->json([], Response::HTTP_OK);
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'message'   => $exception->getMessage(),
                'status'    => $exception->getCode()
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
