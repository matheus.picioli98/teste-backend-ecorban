<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_info'             => 'required|array',
            'user_info.name'        => 'required',
            'user_info.login'       => 'required|unique:users,login',
            'user_info.email'       => 'required|email|unique:users,email',
//            'user_info.password'    => 'required',
            
            'employee_info'                         => 'array',
            'employee_info.description_function'    => 'required',
            'employee_info.salary'                  => 'required',
            'employee_info.date_birthday'           => 'required',
        ];
    }
}
