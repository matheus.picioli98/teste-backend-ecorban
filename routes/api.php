<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('employees')->group(function() {
    Route::get('/{page?}', 'EmployeesController@index')->name('employees.index');
    Route::post('/save', 'EmployeesController@store')->name('employees.save');
    Route::put('/update/{id}', 'EmployeesController@update')->name('employees.update');
    Route::delete('/delete/{id}', 'EmployeesController@destroy')->name('employees.delete');
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
