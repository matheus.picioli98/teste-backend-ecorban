# Teste backend Ecorban

## Environment
* PHP >= 7.2
* Node 10 LTS, npm 6.9
* Composer

## Instruções
* Fazer uma cópia do .env.example e chama-la de .env
* Criar um banco de dados
* No .env, setar as variáveis de banco de dados
* Executar o seguinte comando: ``composer install``
* `php artisan serve` para startar a aplicação
* Acesse localhost:8000
* Login: vitorecorban, senha: 123456

PS: Se for startar de uma outra forma ajustar as variáveis do front-end com o endereço e porta do back-end :)

Após a execução padrão do composer install, ele irá rodar com comandos de build do front-end.
