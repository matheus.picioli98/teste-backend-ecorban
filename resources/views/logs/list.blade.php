@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-2 mt-2 mb-2">
                                <a href="{{ route('dashboard.list') }}" class="btn btn-primary">
                                    <i class="fa fa-chevron-left"></i> Dashboard
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <h3 class="box-title">Logs</h3>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover dataTable" id="table-logs" role="grid">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Classe</th>
                                <th>Evento</th>
                                <th>Dados</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Classe</th>
                                <th>Evento</th>
                                <th>Dados</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($logs as $log)
                                <tr>
                                    <td>{{ $log->id }}</td>
                                    <td>{{ $log->class }} - #{{ $log->class_id }}</td>
                                    <td>{{ $log->event }}</td>
                                    <td>{{ $log->data }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-logs').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                }
            });
        });
    </script>
@stop
